$(function() {
  $.each(nodes, function( index, value ) {
    form = $('<form action="' + value[1] + '" method="post"></form>');
    form.append('<label for="text">' + value[0] + ':</label>');
    form.append('<input type="text" name="text" autocomplete="off">');
    form.append('<button type="submit">Send!</button>');
    $('#wrapper').append(form)
  });
});
